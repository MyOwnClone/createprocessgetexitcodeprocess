#include <Windows.h>
#include <stdio.h>
#include <string>

int executeCommandLineAndReturnErrorCode(std::string filename, std::string parameters);

int main(int argc, char **argv)
{
	int code = executeCommandLineAndReturnErrorCode("returnexitcode.exe", "param");

	printf("code=%d\n", code);

	return 0;
}

int executeCommandLineAndReturnErrorCode(std::string filename, std::string parameters)
{
	DWORD                code;
	STARTUPINFO          si;
	PROCESS_INFORMATION  pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);

	std::string cmdLine = filename;
	cmdLine += " " + parameters;

	TCHAR* szFile = new TCHAR[cmdLine.length()+1]; 

	size_t convertedChars = 0;
	mbstowcs_s(&convertedChars, szFile, cmdLine.length() + 1, cmdLine.c_str(), _TRUNCATE);

	if (!CreateProcess(NULL, szFile, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi)) {
		printf("CreateProcess() failed: %d\n", GetLastError());
		exit(1);
	}

	delete[] szFile;

	if (WaitForSingleObject(pi.hProcess, INFINITE) == WAIT_FAILED) {
		printf("WaitForSingleObject() failed: %d\n", GetLastError());
		exit(1);
	}

	if (!(GetExitCodeProcess(pi.hProcess, &code))) {
		printf("GetExitCodeProcess() failed: %d\n", GetLastError());
		exit(1);
	}

	return code;
}